package com.udacity.sandwichclub.utils;

import android.util.Log;

import com.udacity.sandwichclub.model.Sandwich;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class JsonUtils {
    private static final String TAG = "JsonUtils";

    public static Sandwich parseSandwichJson(String json) {

        // Instantiate a new sandwich object
        Sandwich sandwich = new Sandwich();

        try {
            // Get a json object from string input
            JSONObject jsonSandwich = new JSONObject(json);

            // Set the sandwich object's properties
            JSONObject jsonName = jsonSandwich.getJSONObject("name");
            sandwich.setMainName(jsonName.getString("mainName"));

            // Parse the json array of alias and place them into a list of strings.
            JSONArray aka = jsonName.getJSONArray("alsoKnownAs");
            sandwich.setAlsoKnownAs(jsonArrayToStringArray(aka));

            sandwich.setPlaceOfOrigin(jsonSandwich.optString("placeOfOrigin"));
            sandwich.setDescription(jsonSandwich.getString("description"));
            JSONArray ingredients = jsonSandwich.getJSONArray("ingredients");
            sandwich.setIngredients(jsonArrayToStringArray(ingredients));
            sandwich.setImage(jsonSandwich.optString("image"));

        } catch (JSONException e) {
            // Log and display stack trace
            Log.d(TAG, e.getMessage());
            e.printStackTrace();
        }
        return sandwich;
    }

    // Takes a jsonArray and returns it as an ArrayList of strings.
    private static List<String> jsonArrayToStringArray(JSONArray jsonArray) {
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            strings.add(jsonArray.optString(i));
        }
        return strings;
    }
}
