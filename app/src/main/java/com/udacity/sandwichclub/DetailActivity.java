package com.udacity.sandwichclub;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.udacity.sandwichclub.model.Sandwich;
import com.udacity.sandwichclub.utils.JsonUtils;

import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();

    public static final String EXTRA_POSITION = "extra_position";
    private static final int DEFAULT_POSITION = -1;

    @BindView(R.id.also_known_tv)
    TextView tvAlsoKnownAs;

    @BindView(R.id.origin_tv)
    TextView tvPlaceOfOrigin;

    @BindView(R.id.description_tv)
    TextView tvDescription;

    @BindView(R.id.ingredients_tv)
    TextView tvIngredients;

    @BindView(R.id.image_iv)
    ImageView ingredientsIv;

    Sandwich mSandwich;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent == null) {
            closeOnError();
        }

        int position = 0;
        if (intent != null) {
            position = intent.getIntExtra(EXTRA_POSITION, DEFAULT_POSITION);
        }

        if (position == DEFAULT_POSITION) {
            // EXTRA_POSITION not found in intent
            closeOnError();
            return;
        }

        // Get an array of sandwich details as strings
        String[] sandwiches = getResources().getStringArray(R.array.sandwich_details);
        // Grab the string at 'position'
        String json = sandwiches[position];
        // Convert the string into a sandwich object
        Sandwich sandwich = JsonUtils.parseSandwichJson(json);
        mSandwich = sandwich;

        if (sandwich == null) {
            // Sandwich data unavailable
            closeOnError();
            return;
        }

        populateUI();

        // Really should be logic here to remove the image if it does not exist
        // or at least use a default placeholder image.
        Picasso.with(this)
                .load(sandwich.getImage())
                .into(ingredientsIv);

        setTitle(sandwich.getMainName());
    }

    private void closeOnError() {
        finish();
        Toast.makeText(this, R.string.detail_error_message, Toast.LENGTH_SHORT).show();
    }

    private void populateUI() {
        // really, I think there should be logic here to simply set the visibility
        // of empty fields to "gone" to remove them from the layout.
        tvAlsoKnownAs.setText(formatStringFromList(mSandwich.getAlsoKnownAs()));
        tvPlaceOfOrigin.setText(mSandwich.getPlaceOfOrigin());
        tvDescription.setText(mSandwich.getDescription());

        tvIngredients.setText(formatStringFromList(mSandwich.getIngredients()));
    }

    private String formatStringFromList(List<String> list) {
        String string = "";

        if (list.isEmpty()) {
            return "N/A";
        }

        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            string = string.concat((String) iterator.next());
            if (iterator.hasNext()) {
                string = string.concat("\n");
            }
        }

        return string;
    }
}
